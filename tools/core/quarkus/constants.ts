import { BuilderCommandAliasMapper, GradleBuilder } from '@nxrocks/common';

export const GRADLE_QUARKUS_COMMAND_MAPPER : BuilderCommandAliasMapper = {
  'dev': 'quarkusDev',
  'remoteDev': 'quarkusRemoteDev',
  'test': 'test',
  'clean': 'clean',
  'format': 'spotlessApply',
  'format-check': 'spotlessCheck',
  'build': 'build',
  'package': 'package',
  'addExtension': 'addExtension',
  'listExtensions': 'listExtensions',
}

export const GRADLE_BUILDER = new GradleBuilder(GRADLE_QUARKUS_COMMAND_MAPPER);

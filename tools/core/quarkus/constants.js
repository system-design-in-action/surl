"use strict";
exports.__esModule = true;
exports.GRADLE_BUILDER = exports.GRADLE_QUARKUS_COMMAND_MAPPER = void 0;
var common_1 = require("@nxrocks/common");
exports.GRADLE_QUARKUS_COMMAND_MAPPER = {
    'dev': 'quarkusDev',
    'remoteDev': 'quarkusRemoteDev',
    'test': 'test',
    'clean': 'clean',
    'format': 'spotlessApply',
    'format-check': 'spotlessCheck',
    'build': 'build',
    'package': 'package',
    'addExtension': 'addExtension',
    'listExtensions': 'listExtensions'
};
exports.GRADLE_BUILDER = new common_1.GradleBuilder(exports.GRADLE_QUARKUS_COMMAND_MAPPER);

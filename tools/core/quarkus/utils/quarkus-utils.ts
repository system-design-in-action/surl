import { BuilderCommandAliasType, hasGradleProject, hasMavenProject, runBuilderCommand } from '@nxrocks/common';
import { MAVEN_BUILDER } from '@nxrocks/nx-quarkus/src/core/constants';
import { GRADLE_BUILDER } from '../constants';

const getBuilder = (cwd: string) => {
  if (hasMavenProject(cwd)) return MAVEN_BUILDER;
  if (hasGradleProject(cwd)) return GRADLE_BUILDER;

  throw new Error(
    `Cannot determine the build system. No 'pom.xml' nor 'build.gradle' file found under '${cwd}'`
  );
}

export function runQuarkusPluginCommand(
  commandAlias: BuilderCommandAliasType,
  params: string[],
  options: { cwd?: string; ignoreWrapper?: boolean } = { ignoreWrapper: false },
): { success: boolean } {
  return runBuilderCommand(commandAlias, getBuilder, params, options);
}


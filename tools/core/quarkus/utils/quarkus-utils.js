"use strict";
exports.__esModule = true;
exports.runQuarkusPluginCommand = void 0;
var common_1 = require("@nxrocks/common");
var constants_1 = require("@nxrocks/nx-quarkus/src/core/constants");
var constants_2 = require("../constants");
var getBuilder = function (cwd) {
    if ((0, common_1.hasMavenProject)(cwd))
        return constants_1.MAVEN_BUILDER;
    if ((0, common_1.hasGradleProject)(cwd))
        return constants_2.GRADLE_BUILDER;
    throw new Error("Cannot determine the build system. No 'pom.xml' nor 'build.gradle' file found under '".concat(cwd, "'"));
};
function runQuarkusPluginCommand(commandAlias, params, options) {
    if (options === void 0) { options = { ignoreWrapper: false }; }
    return (0, common_1.runBuilderCommand)(commandAlias, getBuilder, params, options);
}
exports.runQuarkusPluginCommand = runQuarkusPluginCommand;

import * as path from 'path';
import { AddExtensionExecutorOptions } from '@nxrocks/nx-quarkus/src/executors/add-extension/schema';
import { ExecutorContext } from 'nx/src/config/misc-interfaces';
import { runQuarkusPluginCommand } from '../../core/quarkus/utils/quarkus-utils';

export async function addExtensionExecutor(options: AddExtensionExecutorOptions, context: ExecutorContext) {
  const root = path.resolve(context.root, options.root);
  const result = runQuarkusPluginCommand('addExtension', options.args, {
    cwd: root,
    ignoreWrapper: options.ignoreWrapper
  });

  if (!result.success) {
    throw new Error();
  }

  return result;
}

export default addExtensionExecutor;
